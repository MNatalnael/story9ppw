from django.shortcuts import render, redirect
from django.contrib.auth import authenticate, login, logout
# Create your views here.

def index(request):
    context = {
        'sapaan':"Hello, "
    }
    return render(request, 'base.html')

def login_method(request):
    if(request.method == "POST"):
        user_login = request.POST['user']
        pass_login = request.POST['pass']

        user = authenticate(request, username=user_login, password=pass_login)
        if user is not None:
            login(request, user)
        else:
            return redirect("login_method")

        return redirect("index")

    return render(request, 'login.html')

def logout_method(request):
    if(request.method == "POST"):
        if(request.POST["logout"] == ""):
            logout(request)

        return redirect("index")

    return render(request, 'logout.html')
